# Creating Modules

## Chapter 5.7, basically

### Your First Module

You've seen how to use modules like `random`, `math`, and `turtle`, but how do you create a module?

Have you ever written a Python script in PyCharm?  Then you've created a module!

At it's most basic level, a Python module is just a Python source code file.  Let's consider the Python file shown below.

`coffee_shop.py`

---

```python
"""
The coffee shop module contains functions and contains variables
important to implementing a coffee shop.
"""

# Set some variables
shop_name = "Karl's Brew House"
coffee_sizes = ["small", "medium", "large"]
coffee_roasts = ["hot chocolate", "light", "medium", "dark", "espresso"]
```

---


> **Note - you should play along in PyCharm.  Create a new project, and inside it and add the files we're creating here.  Make sure to use the same filenames we do here.**

This is a Python script named `coffee_shop.py` that contains three variables: `shop_name`, `coffee_sizes`, and `coffee_roasts`.  The `shop_name` is a string, `coffee_sizes` is a list containing strings, and `coffee_roasts` is also a list containing strings.

This is great!  We've got the basics of a coffee shop.  All you need is some roasted coffee and cups.  You're good to go.

Well - that's cause this is a `cyber cafe`.  It only exists in cyberspace.  In the real world you also need a location and a crew and capital...  But we're gonna ignore those things.

### Using Your First Module

What can we do with such a `coffee_shop` module?  Well we can import it and use it in other Python source code files.  Let's consider the Python file shown below.

`coffee_customer.py`


---

```python
# Import the module with coffee_shop functionality
import coffee_shop

# Output the information we know from the module
print("Welcome to", coffee_shop.shop_name)
print("Available sizes:", coffee_shop.coffee_sizes)
print("Available roasts:", coffee_shop.coffee_roasts)
```

---


This is a Python script named `coffee_customer.py` that imports our `coffee_shop` module, then prints out the information from that module.

> **Note - these files must be in the same directory on your computer, or in the same PyCharm project.  Python doesn't automatically know how to find them, otherwise.**

We use "dot notation" to grab the `shop_name`, `coffee_sizes`, and `coffee_roasts` variables from the `coffee_shop` module.  Then we print them out as parts of nice messages.

Are variables the only thing we can store in a module then use from a different module?  You already know the answer to this from our other sections, but...

### We Can Also Do Functions

Let's modify our coffee shop!  I added some stuff to our module below.

`coffee_shop.py`


---

```python
"""
The coffee shop module contains functions and contains variables
important to implementing a coffee shop.
"""

# Set some variables
shop_name = "Karl's Brew House"
coffee_sizes = ["small", "medium", "large"]
coffee_roasts = ["hot chocolate", "light", "medium", "dark", "espresso"]

def order_coffee(size, roast):
    """
    Take an order from a user
    :param size: a string containing one of the coffee_sizes
    :param roast: a string containing one of the coffee_roasts
    :return: a message about the coffee order
    """
    return "Here's your {} coffee roasted {}".format(size, roast)
```

---


You'll see all the old file contents, but now there's also an `order_coffee` function that takes two arguments, `size` and `roast`.

Also - look at all the awesome comments in there!

> **Module Comments** - It's important to include header comments in your module that explain what the module does.  Other header information is important to your projects, too.  You should include who created the file, when it was created, and when it was modified.  These should be the first lines of your module just like they are here.

> **Function Comments** - Functions are the next chapter, but the comments I use here are the same style you should be using too.

Ok - so we've got a function in our module now, let's use it.

`coffee_customer.py`


---

```python
# Import the module with coffee_shop functionality
import coffee_shop

# Output the information we know from the module
print("Welcome to", coffee_shop.shop_name)
print("Available sizes:", coffee_shop.coffee_sizes)
print("Available roasts:", coffee_shop.coffee_roasts)

# Get some inputs from the user
order_size = input("What size coffee do you want? ")
order_roast = input("What roast do you want? ")

# Send the order to the coffee shop module
shop_says = coffee_shop.order_coffee(order_size, order_roast)
# Print out whatever it gave back to us
print(shop_says)
```

---


We added some lines to our `coffee_customer` script...  Now after printing data nicely, `coffee_customer` asks the user for a size and a roast.  These are the parameters required by our `order_coffee` function over in the `coffee_shop` module!

You call that `order_coffee` function with "dot notation".  Look at it up there - it's the line that says `shop_says = coffee_shop.order_coffee(order_size, order_roast)`.  The function returns something, so we save that off in `shop_says`.  The next line prints out whatever the shop said.

### But - I Wanted Milk in Mine!

Coffee shops do more than just coffee!  Starbucks sells more milk than coffee...

You know, coffee has 0 calories.  Until you add milk and sugar.  I mean I like milk in mine sometimes, but black is great.  Is that just me?

Maybe you want some milk.  We need to add some functionality to our coffee shop.  Check it out below.

`coffee_shop.py`


---

```python
"""
The coffee shop module contains functions and contains variables
important to implementing a coffee shop.
"""

# Set some variables
shop_name = "Karl's Brew House"
coffee_sizes = ["small", "medium", "large"]
coffee_roasts = ["hot chocolate", "light", "medium", "dark", "espresso"]

def order_coffee(size, roast):
    """
    Take an order from a user
    :param size: a string containing one of the coffee_sizes
    :param roast: a string containing one of the coffee_roasts
    :return: a message about the coffee order
    """
    return "Here's your {} coffee roasted {}".format(size, roast)

def add_milk_please(fat_content):
    """
    Pretend like we're adding some milk to a coffee
    :param fat_content: a string or integer containing the milkfat content
    :return: a message about having added the milk
    """
    return "I've added the {}% milk".format(fat_content)
```

---


Do you see that function we added in there?  It's called `add_milk_please` and it takes one parameter - the `fat_content`.  It returns a string explaining what's up.

This is great.  But the function isn't going to do anything by itself.  We have to call it.  Check out the update to our `coffee_customer` script below.

`coffee_customer.py`


---

```python
# Import the module with coffee_shop functionality
import coffee_shop

# Output the information we know from the module
print("Welcome to", coffee_shop.shop_name)
print("Available sizes:", coffee_shop.coffee_sizes)
print("Available roasts:", coffee_shop.coffee_roasts)

# Get some inputs from the user
order_size = input("What size coffee do you want? ")
order_roast = input("What roast do you want? ")

# Send the order to the coffee shop module
shop_says = coffee_shop.order_coffee(order_size, order_roast)
# Print out whatever it gave back to us
print(shop_says)

# See if the user wants to add milk
add_milk_response = input("Do you want to add milk (y/n)? ")
# Convert the response to lowercase, then check for a "yes" answer
if "y" in add_milk_response.lower():
    milk_fat = input("What percent milk do you want added? ")
    shop_says = coffee_shop.add_milk_please(milk_fat)
    # Print out whatever it gave back to us
    print(shop_says)
```

---


WOW!  That got fancy didn't it?  I mean, we were just ordering coffee..  Now the user can choose to add milk?  Selection is in a couple chapters, but if you read that code like english you're gonna see what's going on.

> **Note: This might be a good time to update your scripts in PyCharm and try out what's going on here.  Pasting these contents in over your old file contents should do it.  Make sure you got it pasted right, and you've got the right file names.**

The call to `add_milk_please` happens right in there - it looks just like the other one: `shop_says = coffee_shop.add_milk_please(milk_fat)`.

### Let's Make Like a Tree and Leaf

Let's wrap this coffee shop visit up.  But - you better leave a tip.  We'll add another function to our coffee shop to enable that.

`coffee_shop.py`


---

```python
"""
The coffee shop module contains functions and contains variables
important to implementing a coffee shop.
"""

# Set some variables
shop_name = "Karl's Brew House"
coffee_sizes = ["small", "medium", "large"]
coffee_roasts = ["hot chocolate", "light", "medium", "dark", "espresso"]

def order_coffee(size, roast):
    """
    Take an order from a user
    :param size: a string containing one of the coffee_sizes
    :param roast: a string containing one of the coffee_roasts
    :return: a message about the coffee order
    """
    return "Here's your {} coffee roasted {}".format(size, roast)

def add_milk_please(fat_content):
    """
    Pretend like we're adding some milk to a coffee
    :param fat_content: a string or integer containing the milkfat content
    :return: a message about having added the milk
    """
    return "I've added the {}% milk".format(fat_content)

def give_tip(tip_amount):
    """
    Take a tip from the user, then be happy about it
    :param tip_amount: the tip amount
    :return: nothing
    """
    print("Thank you so much!  We don't make a ton of money.")

    # Not having a "return" statement causes our function to return None
```

---


We added the `give_tip` function which takes one parameter, the `tip_amount`.  We don't actually do anything with that parameter...  But if we were getting fancier with the coffee shop we might add it to the customer's bill.  We might print it out.  We might berate them for being too cheap...

Here we just go ahead and blurt out a thanks to the user!  Bein' friendly.

How do we call this from our `coffee_customer` script?

`coffee_customer.py`


---

```python
# Import the module with coffee_shop functionality
import coffee_shop

# Output the information we know from the module
print("Welcome to", coffee_shop.shop_name)
print("Available sizes:", coffee_shop.coffee_sizes)
print("Available roasts:", coffee_shop.coffee_roasts)

# Get some inputs from the user
order_size = input("What size coffee do you want? ")
order_roast = input("What roast do you want? ")

# Send the order to the coffee shop module
shop_says = coffee_shop.order_coffee(order_size, order_roast)
# Print out whatever it gave back to us
print(shop_says)

# See if the user wants to add milk
add_milk_response = input("Do you want to add milk (y/n)? ")
# Convert the response to lowercase, then check for a "yes" answer
if "y" in add_milk_response.lower():
    milk_fat = input("What percent milk do you want added? ")
    shop_says = coffee_shop.add_milk_please(milk_fat)
    # Print out whatever it gave back to us
    print(shop_says)

# They better give a tip...
print("THAT'S GOOD COFFEE!  Very good.  Your brain is working again.")
print("You better give a tip.")
tip_amount = input("Tip amount? ")
coffee_shop.give_tip(tip_amount)
```

---


Our function call is there on the last line.

### Summary

You just learned the following things:

* How to create a module: a module is just a Python file.  The examples above actually created two modules!
* How to import functionality from another module: use an `import` statement
* How to use variables from another module: use "dot notation"
* How to use functions from another module: use "dot notation"
* Karl takes his coffee black: good coffee is good.

If you've got more questions about functions, the next chapter will go over those.  If you've got more questions about that "if" statement in there (on the `add_milk_response` call line) we're gonna cover those in the "selection" chapter.
