# Publicly-accessible CS1181 Materials

Publicly-accessible materials for the ISU CS 1181 course.

Do not add things you want kept private.

## creating\_modules.py

Do not update this file here, update it in the private CS 1181 repo then copy it here
